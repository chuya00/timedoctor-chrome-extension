(function(angular) {
  'use strict';

  angular.module('app', [
    /* Shared modules */
    'app.core',

    /* Features */
  ]);

})(angular);

(function(angular) {
  'use strict';

  angular.module('app.core', [
    //'ngResource',
    'ngSanitize',
    'ui.router',
    'satellizer',
    'ngAnimate',
    'toaster',
    'ngMaterial'
  ]);

})(angular);

(function(angular) {
  'use strict';

  angular.module('app')
    .config(config)
    .run(run);

  config.$inject = ['$authProvider', '$compileProvider'];

  /* @ngInject */
  function config($authProvider, $compileProvider) {
    $compileProvider.aHrefSanitizationWhitelist(
      /^\s*(https?|ftp|mailto|chrome-extension):/);
  }

  run.$inject = ['$rootScope', '$state', '$auth'];
  /* @ngInject */
  function run($rootScope, $state, $auth) {

    chrome.runtime.onMessage.addListener(
      function(request, sender, sendResponse) {
        if (request.greeting == "setToken") {
          chrome.storage.sync.get("responseUrl", function(item) {
            if (angular.isDefined(item.responseUrl)) {
              var responseUrl = item.responseUrl;
              var token = getParameterByName('access_token',
                responseUrl);
              $auth.setToken(token);
              $state.go('list');
            }

          });
        }

      });

    $rootScope.$on('$stateChangeStart', function(event, toState, toParams,
      fromState, fromParams) {
      if ($auth.getToken() == null) {
        chrome.storage.sync.get("responseUrl", function(item) {
          if (angular.isDefined(item.responseUrl)) {
            var responseUrl = item.responseUrl;
            var token = getParameterByName('access_token',
              responseUrl);
            $auth.setToken(token);

            $state.go('list');
          } else {
            $state.go('auth');
          }

        });
      }

    });

  }

  function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
      results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
  }
})(angular);

(function(angular) {
  'use strict';

  angular.module('app')
    .config(config);

  config.$inject = ['$stateProvider', '$urlRouterProvider'];

  /* @ngInject */
  function config($stateProvider, $urlRouterProvider) {

    // For any unmatched url, redirect to /
    $urlRouterProvider.otherwise("/list");

    //////STATES//////
    var auth = {
      name: "auth",
      url: "/auth",
      views: {
        "main": {
          templateUrl: "./app/auth/auth.html",
          controller: "AuthController",
          controllerAs: "vm"
        }
      }
    };

    var list = {
      name: "list",
      url: "/list",
      views: {
        "main": {
          templateUrl: "./app/user/list.html",
          controller: "ListController",
          controllerAs: "vm"
        }
      }
    };

    var users = {
      name: "users",
      url: "/users/:id",
      views: {
        "main": {
          templateUrl: "./app/user/users.html",
          controller: "UsersController",
          controllerAs: "vm"
        }
      }
    };

    var tasks = {
      name: "tasks",
      url: "/tasks/:company_id/:user_id/:user_name",
      views: {
        "main": {
          templateUrl: "./app/user/tasks.html",
          controller: "TasksController",
          controllerAs: "vm"
        }
      }
    };

    ////////////

    $stateProvider
      .state(auth)
      .state(list)
      .state(users)
      .state(tasks);

  }

})(angular);

(function(angular) {
  'use strict';

  angular.module('app')
    .controller('AuthController', AuthController);

  AuthController.$inject = ['$scope', '$auth', '$state']
    /* @ngInject */
  function AuthController($scope, $auth, $state) {
    var vm = this;

    vm.authenticate = authenticate;

    ////////////////

    function authenticate() {
      chrome.runtime.sendMessage({
        greeting: "authenticate"
      }, function(response) {
        console.log(response);
      });
    }

  }

})(angular);

(function() {
  'use strict';

  angular.module('app')
    .controller('ListController', ListController);

  ListController.$inject = ['UserService', '$state']
    /* @ngInject */
  function ListController(UserService, $state) {
    var vm = this;

    vm.companies = [];

    activate();

    //////////

    function activate() {
      UserService.getCompanies().then(function(data) {
        vm.companies = data;
      });
    }

  }

})();

(function() {
  'use strict';

  angular.module('app')
    .controller('TasksController', TasksController);

  TasksController.$inject = ['UserService', '$stateParams']
    /* @ngInject */
  function TasksController(UserService, $stateParams) {
    var vm = this;

    vm.company_id = $stateParams.company_id;
    vm.user_id = $stateParams.user_id;
    vm.user_name = $stateParams.user_name;
    vm.tasks = [];

    activate();

    //////////

    function activate() {
      UserService.getTasks(vm.company_id, vm.user_id).then(function(data) {
        vm.tasks = data;
      });
    }

  }

})();

(function() {
  'use strict';

  angular.module('app')
    .factory('UserService', UserService);

  UserService.$inject = ['$http', '$q'];

  /* @ngInject */
  function UserService($http, $q) {
    var api = 'https://webapi.timedoctor.com';
    var d = $q.defer();

    var service = {
      getCompanies: getCompanies,
      getUsers: getUsers,
      getTasks: getTasks
    }

    return service;

    ////////////////

    function getTasks(company_id, user_id) {
      var d = $q.defer();
      var endpoint = api + '/v1.1/companies/' + company_id + '/users/' +
        user_id + '/tasks';
      $http.get(endpoint)
        .then(function(resp) {
          d.resolve(resp.data.tasks);
        })
        .catch(function(error) {
          console.log(error);
          d.reject(error);
        });

      return d.promise;
    }

    function getUsers(id) {
      var d = $q.defer();
      var endpoint = api + '/v1.1/companies/' + id + '/users';
      $http.get(endpoint)
        .then(function(resp) {
          d.resolve(resp.data.users);
        })
        .catch(function(error) {
          console.log(error);
          d.reject(error);
        });

      return d.promise;
    }

    function getCompanies() {
      var d = $q.defer();
      var endpoint = api + '/v1.1/companies';
      $http.get(endpoint)
        .then(function(resp) {
          d.resolve(resp.data.accounts);
        })
        .catch(function(error) {
          console.log(error);
          d.reject(error);
        });

      return d.promise;
    }
  }

})();

(function() {
  'use strict';

  angular.module('app')
    .controller('UsersController', UsersController);

  UsersController.$inject = ['UserService', '$stateParams']
    /* @ngInject */
  function UsersController(UserService, $stateParams) {
    var vm = this;

    vm.company_id = $stateParams.id;
    vm.users = [];

    activate();

    //////////

    function activate() {
      UserService.getUsers(vm.company_id).then(function(data) {
        vm.users = data;
      });
    }

  }

})();
