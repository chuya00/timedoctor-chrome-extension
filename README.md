How to install:

1. Clone repository: https://bitbucket.org/chuya00/timedoctor-chrome-extension

2. Open chrome browser.

3. Click the 3 vertical dots icon at the top right corner of the window.

4. Go to more tools -> Extensions

5. Make sure you are in developer mode. Click the developer mode checkbox at the top right corner.

6. Click unpacked extension.

7. Select the timedoctor folder then click select.

8. Go to https://webapi.timedoctor.com/app

9. Login using this credential. username: dodong000@gmail.com password: jellanpassword123

10. Click the edit hyperlink.

11. Change redirect URL from this: https://edojbgklfkdlcckghbibanpgpfnkjahh.chromiumapp.org/ to https://<his_extension_id>.chromiumapp.org/. This can be found in extensions page. It is labelled as "ID".

12. At this step, the chrome extension should be added into your chrome browser.

You can also install the extension from chrome webstore:
https://chrome.google.com/webstore/detail/timedoctor-test/lknbjfpnfhjofjmaihghmkighdneddhh