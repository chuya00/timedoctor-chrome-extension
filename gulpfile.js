var gulp = require('gulp');
var concat = require('gulp-concat');

gulp.task('scripts', function() {
  return gulp.src([
      // './lib/jquery/dist/jquery.min.js',
      // './lib/bootstrap/dist/js/bootstrap.min.js',
      // './lib/angular/angular.min.js',
      // './lib/angular-animate/angular-animate.min.js',
      // './lib/angularjs-toaster/toaster.min.js',
      // './lib/angular-ui-router/release/angular-ui-router.min.js',
      // './lib/angular-sanitize/angular-sanitize.min.js',
      // './node_modules/angular-resource/angular-resource.min.js',
      // //'./lib/angular-aria/angular-aria.min.js',
      // './lib/angular-material/angular-material.min.js',
      // './node_modules/satellizer/dist/satellizer.min.js',

      //Core files
      './app/core/app.module.js',
      './app/core/app.core.js',
      './app/core/app.config.js',
      './app/core/app.route.js',

      //Login module
      './app/auth/*.js',

      //user module
      './app/user/*.js',
    ])
    .pipe(concat('app.js'))
    .pipe(gulp.dest('./dist/js/'));
});
