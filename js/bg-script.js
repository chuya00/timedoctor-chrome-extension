chrome.runtime.onMessage.addListener(
  function(request, sender, sendResponse) {
    if (request.greeting == "authenticate") {
      authenticate();
    }

  });

function authenticate() {
  var clientId = '719_rykcuo7t9e8cosowww8k040gwkook48kcg0owso8kkow0s4c0';
  var redirectUri =
    'https://' + chrome.runtime.id + '.chromiumapp.org/';
  chrome.identity.launchWebAuthFlow({
    url: 'https://webapi.timedoctor.com/oauth/v2/auth?client_id=' +
      clientId + '&redirect_uri=' + encodeURIComponent(redirectUri) +
      '&response_type=token',
    interactive: true
  }, function(responseUrl) {

    chrome.storage.sync.set({
      'responseUrl': responseUrl
    });

    chrome.runtime.sendMessage({
      greeting: "setToken"
    }, function(response) {
      console.log(response);
    });

  })
}
