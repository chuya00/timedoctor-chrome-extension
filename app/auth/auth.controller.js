(function(angular) {
  'use strict';

  angular.module('app')
    .controller('AuthController', AuthController);

  AuthController.$inject = ['$scope', '$auth', '$state']
    /* @ngInject */
  function AuthController($scope, $auth, $state) {
    var vm = this;

    vm.authenticate = authenticate;

    ////////////////

    function authenticate() {
      chrome.runtime.sendMessage({
        greeting: "authenticate"
      }, function(response) {
        console.log(response);
      });
    }

  }

})(angular);
