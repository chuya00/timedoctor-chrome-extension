(function() {
  'use strict';

  angular.module('app')
    .controller('ListController', ListController);

  ListController.$inject = ['UserService', '$state']
    /* @ngInject */
  function ListController(UserService, $state) {
    var vm = this;

    vm.companies = [];

    activate();

    //////////

    function activate() {
      UserService.getCompanies().then(function(data) {
        vm.companies = data;
      });
    }

  }

})();
