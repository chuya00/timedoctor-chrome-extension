(function() {
  'use strict';

  angular.module('app')
    .factory('UserService', UserService);

  UserService.$inject = ['$http', '$q'];

  /* @ngInject */
  function UserService($http, $q) {
    var api = 'https://webapi.timedoctor.com';
    var d = $q.defer();

    var service = {
      getCompanies: getCompanies,
      getUsers: getUsers,
      getTasks: getTasks
    }

    return service;

    ////////////////

    function getTasks(company_id, user_id) {
      var d = $q.defer();
      var endpoint = api + '/v1.1/companies/' + company_id + '/users/' +
        user_id + '/tasks';
      $http.get(endpoint)
        .then(function(resp) {
          d.resolve(resp.data.tasks);
        })
        .catch(function(error) {
          console.log(error);
          d.reject(error);
        });

      return d.promise;
    }

    function getUsers(id) {
      var d = $q.defer();
      var endpoint = api + '/v1.1/companies/' + id + '/users';
      $http.get(endpoint)
        .then(function(resp) {
          d.resolve(resp.data.users);
        })
        .catch(function(error) {
          console.log(error);
          d.reject(error);
        });

      return d.promise;
    }

    function getCompanies() {
      var d = $q.defer();
      var endpoint = api + '/v1.1/companies';
      $http.get(endpoint)
        .then(function(resp) {
          d.resolve(resp.data.accounts);
        })
        .catch(function(error) {
          console.log(error);
          d.reject(error);
        });

      return d.promise;
    }
  }

})();
