(function() {
  'use strict';

  angular.module('app')
    .controller('TasksController', TasksController);

  TasksController.$inject = ['UserService', '$stateParams']
    /* @ngInject */
  function TasksController(UserService, $stateParams) {
    var vm = this;

    vm.company_id = $stateParams.company_id;
    vm.user_id = $stateParams.user_id;
    vm.user_name = $stateParams.user_name;
    vm.tasks = [];

    activate();

    //////////

    function activate() {
      UserService.getTasks(vm.company_id, vm.user_id).then(function(data) {
        vm.tasks = data;
      });
    }

  }

})();
