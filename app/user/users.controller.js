(function() {
  'use strict';

  angular.module('app')
    .controller('UsersController', UsersController);

  UsersController.$inject = ['UserService', '$stateParams']
    /* @ngInject */
  function UsersController(UserService, $stateParams) {
    var vm = this;

    vm.company_id = $stateParams.id;
    vm.users = [];

    activate();

    //////////

    function activate() {
      UserService.getUsers(vm.company_id).then(function(data) {
        vm.users = data;
      });
    }

  }

})();
