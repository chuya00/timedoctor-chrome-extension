(function(angular) {
  'use strict';

  angular.module('app.core', [
    //'ngResource',
    'ngSanitize',
    'ui.router',
    'satellizer',
    'ngAnimate',
    'toaster',
    'ngMaterial'
  ]);

})(angular);
