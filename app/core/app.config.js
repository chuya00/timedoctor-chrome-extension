(function(angular) {
  'use strict';

  angular.module('app')
    .config(config)
    .run(run);

  config.$inject = ['$authProvider', '$compileProvider'];

  /* @ngInject */
  function config($authProvider, $compileProvider) {
    $compileProvider.aHrefSanitizationWhitelist(
      /^\s*(https?|ftp|mailto|chrome-extension):/);
  }

  run.$inject = ['$rootScope', '$state', '$auth'];
  /* @ngInject */
  function run($rootScope, $state, $auth) {

    chrome.runtime.onMessage.addListener(
      function(request, sender, sendResponse) {
        if (request.greeting == "setToken") {
          chrome.storage.sync.get("responseUrl", function(item) {
            if (angular.isDefined(item.responseUrl)) {
              var responseUrl = item.responseUrl;
              var token = getParameterByName('access_token',
                responseUrl);
              $auth.setToken(token);
              $state.go('list');
            }

          });
        }

      });

    $rootScope.$on('$stateChangeStart', function(event, toState, toParams,
      fromState, fromParams) {
      if ($auth.getToken() == null) {
        chrome.storage.sync.get("responseUrl", function(item) {
          if (angular.isDefined(item.responseUrl)) {
            var responseUrl = item.responseUrl;
            var token = getParameterByName('access_token',
              responseUrl);
            $auth.setToken(token);

            $state.go('list');
          } else {
            $state.go('auth');
          }

        });
      }

    });

  }

  function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
      results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
  }
})(angular);
