(function(angular) {
  'use strict';

  angular.module('app')
    .config(config);

  config.$inject = ['$stateProvider', '$urlRouterProvider'];

  /* @ngInject */
  function config($stateProvider, $urlRouterProvider) {

    // For any unmatched url, redirect to /
    $urlRouterProvider.otherwise("/list");

    //////STATES//////
    var auth = {
      name: "auth",
      url: "/auth",
      views: {
        "main": {
          templateUrl: "./app/auth/auth.html",
          controller: "AuthController",
          controllerAs: "vm"
        }
      }
    };

    var list = {
      name: "list",
      url: "/list",
      views: {
        "main": {
          templateUrl: "./app/user/list.html",
          controller: "ListController",
          controllerAs: "vm"
        }
      }
    };

    var users = {
      name: "users",
      url: "/users/:id",
      views: {
        "main": {
          templateUrl: "./app/user/users.html",
          controller: "UsersController",
          controllerAs: "vm"
        }
      }
    };

    var tasks = {
      name: "tasks",
      url: "/tasks/:company_id/:user_id/:user_name",
      views: {
        "main": {
          templateUrl: "./app/user/tasks.html",
          controller: "TasksController",
          controllerAs: "vm"
        }
      }
    };

    ////////////

    $stateProvider
      .state(auth)
      .state(list)
      .state(users)
      .state(tasks);

  }

})(angular);
